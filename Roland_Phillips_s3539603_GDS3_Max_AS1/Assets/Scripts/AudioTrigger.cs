﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrigger : MonoBehaviour
{

     void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "ENV")
          
            {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            Debug.Log("trigger sound");
            }
        
    }
}
