﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerSci : MonoBehaviour {

    public void Gameplay() {
        SceneManager.LoadScene("Gameplay");
        Debug.Log("Gameplay");
    }

    public void Menu() {
        SceneManager.LoadScene("Menu");
        Debug.Log("Menu");

    }

    public void Quit() {

        Application.Quit();
    }
    

}
