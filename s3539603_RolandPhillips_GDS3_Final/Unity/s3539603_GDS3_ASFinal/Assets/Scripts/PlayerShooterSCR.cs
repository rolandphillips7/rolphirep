﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooterSCR : MonoBehaviour {
	public GameObject playerBullet;
	public Transform FirePoint;
	public float bulSpeed;

	public bool isFiring;
	// Use this for initialization
	void Start() {
		isFiring = false;

	}

	// Update is called once per frame
	void FixedUpdate() {
		if (Input.GetMouseButtonDown(0)) {
			Debug.Log("Mouse Click");
			//Create bullet.
			GameObject bullet = Instantiate(playerBullet, FirePoint.position, Quaternion.identity) as GameObject;
			//Properl bullet in the direction the gun faces.
			bullet.GetComponent<Rigidbody> ().AddForce (FirePoint.forward * bulSpeed);

		}

	}

	private void Update()
	{

	}
}
