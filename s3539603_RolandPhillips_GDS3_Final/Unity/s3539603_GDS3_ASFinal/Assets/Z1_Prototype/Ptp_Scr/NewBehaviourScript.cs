﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {
    public Transform originalObject;
    public Transform reflectedObject;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        reflectedObject.position = Vector3.Reflect(originalObject.position, Vector3.right);
    }
}
