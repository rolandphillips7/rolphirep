﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ptp_Shooter : MonoBehaviour {
    public GameObject playerBullet;
    //public Rigidbody bulletRB;
    public Transform FirePoint;
	public float bulSpeed;
  
    public bool isFiring;
    // Use this for initialization
    void Start() {
        isFiring = false;

    }

    // Update is called once per frame
	void FixedUpdate() {
        if (Input.GetMouseButtonDown(0)) {
            Debug.Log("Mouse Click");
			//Instantiate(playerBullet, FirePoint.position, FirePoint.rotation);
			//bulletRB.velocity = FirePoint.forward * bulSpeed;
           // isFiring = true;
			GameObject bullet = Instantiate(playerBullet, FirePoint.position, Quaternion.identity) as GameObject;

			bullet.GetComponent<Rigidbody> ().AddForce (FirePoint.forward * bulSpeed);

        }

    }

    private void Update()
    {
       
    }

	//IEnumerator ShootFunction () {
		
	//}


}
